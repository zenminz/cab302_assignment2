package Stock;



/**
 * This method updates the store's inventory after sale and after manifest
 * 
 * @author Minh
 *
 */
public class Updater {
	
	private Stocks stock;
	private Stocks listOfItem;
	
	/**
	 * Constructor to initialize the store's inventory and list of item
	 * in this class
	 * @param stock which is the current stock level
	 * @param listOfItem which can be a sale log or a manifest's list of items
	 */
	public Updater(Stocks stock, Stocks listOfItem) {
		this.stock = stock;
		this.listOfItem = listOfItem;
	}
	
	/**
	 * This method updates the store's inventory after importing
	 * the sale log. This method goes through every item in the inventory and
	 * update it's quantity with a new quantity after subtracting the item's quantity 
	 * in the inventory with the item's quantity in the sale log.
	 */
	public void updateStockAfterSale() {
		
		int quantity;
		Item itemFromStock;
		Item itemFromList;
		for (int i = 0; i < stock.getSize(); i++) {
			itemFromStock = stock.getItem(i);
			for (int j = 0; j < listOfItem.getSize(); j++) {
				itemFromList = listOfItem.getItem(j);
				if (itemFromStock.name().equals(itemFromList.name())) {
					quantity = itemFromStock.quantity() - itemFromList.quantity();
					itemFromStock.updateQuantity(quantity);
					//break;
				}
			}
		}
	}
	
	/**
	 * This method updates the store's inventory after importing
	 * the manifest. This method goes through every item in the inventory and
	 * update it's quantity with new quantity after adding the item's quantity
	 * in the inventory with the item's quantity in the manifest.
	 */
	public void updateStockAfterManifest() {
		int quantity;
		Item itemFromStock;
		Item itemFromList;
		for (int i = 0; i < stock.getSize(); i++) {
			itemFromStock = stock.getItem(i);
			for (int j = 0; j < listOfItem.getSize(); j++) {
				itemFromList = listOfItem.getItem(j);
				if (itemFromStock.name().equals(itemFromList.name())) {
					quantity = itemFromStock.quantity() + itemFromList.quantity();
					itemFromStock.updateQuantity(quantity);
					//break;
				}
			}
		}
	}
	
	/**
	 * This method returns the updated inventory
	 * 
	 * @return the updated inventory
	 */
	public Stocks updatedInventory() {
		return stock;

}
}