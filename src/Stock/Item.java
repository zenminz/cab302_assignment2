package Stock;

import java.util.Comparator;

/**
 * A class to create the item object.
 * 
 * This class contain common attributes for an item
 * such as: name, manufacturing cost, sell price, reorder point, reorder amount, 
 * temperature (in C that must be maintained for item to not perish) and quantity.
 * 
 * @author Minh Pham
 *
 */
public class Item {
	private String name;
	private int manufacturingCost;
	private int sellPrice;
	private int reorderPoint;
	private int reorderAmount;
	private int temp;
	private int quantity;
	
	/**
	 * Constructor for creating an item and initializing common properties for an item
	 * 
	 * @param name of the item
	 * @param manufacturingCost of the item
	 * @param sellPrice of the item
	 * @param reorderPoint of the item
	 * @param reorderAmount of the item
	 * @param temp (temperature) of the item
	 * @param quantity of the item
	 */
	public Item (String name, int manufacturingCost, int sellPrice, int reorderPoint, int reorderAmount, int temp, int quantity) {
		this.name = name;
		this.manufacturingCost = manufacturingCost;
		this.sellPrice = sellPrice;
		this.reorderPoint = reorderPoint;
		this.reorderAmount = reorderAmount;
		this.temp = temp;
		this.quantity = quantity;
	}
	
	/**
	 * A copy constructor, which is used to clone an item with the
	 * exact same properties as that item by passing that item
	 * to this constructor when create new item
	 * 
	 * @param item which needs to be cloned
	 */
	public Item (Item item) {
		this(item.name(), item.manufacturingCost(), item.sellPrice(), item.reorderPoint(), item.reorderAmount(), item.temperature(), item.quantity());
	}
	
	/**
	 * This method return the item's name
	 * 
	 * @return item's name
	 */
	public String name() {
		return name;
	}
	
	/**
	 * This method return the item's manufacturing cost.
	 * (Cost it takes to refill this item)
	 * 
	 * @return item's manufacturing cost
	 */
	public int manufacturingCost() {
		return manufacturingCost;
	}
	
	
	/**
	 * This method return the item's sell price
	 * 
	 * @return item's sell price
	 */
	public int sellPrice() {
		return sellPrice;
	}
	
	/**
	 * This method return item's reorder point.
	 * (At this point, the store's inventory needs to be replenished)
	 * 
	 * @return item's reorder point
	 */
	public int reorderPoint() {
		return reorderPoint;
	}
	
	/**
	 * This method return item's reorder amount.
	 * If an item needs to be reordered, N units of that item are added
	 * to the stock order, where N is the item's reorder amount. 
	 * 
	 * @return item's reorder amount
	 */
	public int reorderAmount() {
		return reorderAmount;
	}
	
	/**
	 * This method return item's temperature
	 * Certain items must be temperature controlled throughout
	 * the entire supply chain.
	 * @return item's temperature
	 */
	public int temperature() {
		return temp;
	}
	
	/**
	 * This method return item's quantity in the store's inventory.
	 * 
	 * @return item's quantity
	 */
	public int quantity() {
		return quantity;
	}
	
	/**
	 * This method updates the quantity of an item in the store's inventory.
	 * This method sets the quantity of that item to the input quantity value.
	 * @param quantity which needs to be updated to
	 */
	public void updateQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	/**
	 * This method overrides the compare method of Comparator for sorting.
	 * When called with Collections.sort(arrayList, name-of-this-method),
	 * the sort method will sort the array list of item in ascending order
	 * based on the item's name.
	 * 
	 * @author Chaitanya Singh
	 * {https://beginnersbook.com/2013/12/java-arraylist-of-object-sort-example-comparable-and-comparator/}
	 */
	
	//https://beginnersbook.com/2013/12/java-arraylist-of-object-sort-example-comparable-and-comparator/ 
	// by CHAITANYA SINGH
    public static Comparator<Item> ItemNameComparator = new Comparator<Item>() {
    		public int compare(Item i1, Item i2) {
    			String ItemName1 = i1.name().toUpperCase();
    			String ItemName2 = i2.name().toUpperCase();

    			//ascending order
    			return ItemName1.compareTo(ItemName2);

    			//descending order
    			//return StudentName2.compareTo(StudentName1);
    		}
    };

}
