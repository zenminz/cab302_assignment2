package Stock;

import java.util.ArrayList;

/**
 * A class to store a collection of items. This class can be used for representing
 * store inventory, stock orders, sales logs and truck cargo.
 * 
 * @author Minh
 *
 */
public class Stocks {
	private ArrayList<Item> Items;
	private Stocks stock;
	
	/**
	 * Constructor to create a new class object to store items. This constructor initializes
	 * a an empty array list to store Items.
	 */
	public Stocks () {
		Items = new ArrayList<Item>();
	}
	
	/**
	 * Copy constructor, that is used to clone an exist Stocks. This also clone every
	 * item inside that Stocks. 
	 * @param stock which is class need to be cloned
	 */
	public Stocks (Stocks stock) {
		Items = new ArrayList<Item>(stock.getSize());
		Item item1;
		for (Item item : stock.stocks()) {
			item1 = new Item(item);
			Items.add(item1);
		}
		//Items = new ArrayList<Item>(stock.stocks());
	}
	
	/**
	 * Copy constructor, that is used to clone an exist Stocks. This also clone 
	 * every item inside the Stocks.
	 * @param Items list of the Stocks class which needs to be cloned
	 */
	public Stocks(ArrayList<Item> Items) {
		this.Items = new ArrayList<Item>();
		//Item item1;
		for (Item item : Items) {
			//item1 = new Item(item);
			this.Items.add(new Item(item));
		}
	}
	
	/**
	 * This method add an item into the list. This method takes an item as input
	 * then add that item to the list.
	 * 
	 * @param item needs to be added
	 */
	public void addItem(Item item) {
		Items.add(item);
	}

	/**
	 * This method remove an item from the list at input index. This method takes
	 * an item index as input then removes the item at that index from the list.
	 * @param index of the item needs to be removed
	 */
	public void remove(int index) {
		Items.remove(index);
	}
	
	/**
	 * THis methods return an item from the list at the value of the input index position.
	 * This method takes an index as input
	 * 
	 * @param index of the needed item
	 * @return an item at the input index
	 */
	public Item getItem(int index) {
		return Items.get(index);
	}
	
	/**
	 * This method returns how many items in the list, which
	 * is the number of distinct items in the list
	 * 
	 * @return the number of distinct items in the list
	 */
	public int getSize() {
		return Items.size();
		
	}
	
	/**
	 * This method returns a whole list of the items
	 * 
	 * @return list of items
	 */
	public ArrayList<Item> stocks () {
		return Items;
	}
	
	/**
	 * This method returns total quantity of items in the list. This
	 * method loops through every item in the list and get that item's quantity.
	 * The method then add that item's quantity to the totalQuantity variable.
	 * 
	 * @return total quantity of items in the list
	 */
	public int totalQuantity() {
		int totalQuantity = 0;
		for (Item item : Items) {
			totalQuantity += item.quantity();
		}
		return totalQuantity;
	}
	
	/**
	 * This method returns the total cost of the list. THis method
	 * loops through every item in the list, get that item's quantity 
	 * and manufacturing cost. The method then multiply item's quantity 
	 * with its manufacturing cost, then add the result to the totalCost
	 * variable.
	 * 
	 * @return total cost of the list
	 */
	public double totalCost() {
		int totalCost = 0;
		for (Item item : Items) {
			totalCost += item.quantity() * item.manufacturingCost();
		}
		return totalCost;
	}
	
	/**
	 * This method returns the total sell price of the list. THis method
	 * calculate the total sell price of each item by multiplying that item's
	 * quantity with its sell price, then add those sell price together 
	 * @return total sell price of the list
	 */
	public double totalSell() {
		double totalSell = 0;
		for (Item item : Items) {
			totalSell += item.quantity() * item.sellPrice();
		}
		return totalSell;
	}
	
	/**
	 * THis method returns the position of an item which has a name that matches
	 * the input. This loop through every item in the list and compares that item's name
	 * with the input name. The method returns the position if matches and -1 if 
	 * nothing matches
	 * 
	 * @param name of the item needs to be searched
	 * @return position if found and -1 if not found
	 */
	public int itemPosition(String name) {
		int counter = 0;
		for (Item item : Items) {
			if (item.name().equals(name)) {
				return counter; // found the item that matches the input
			}
			counter += 1;
		}
		return -1; // not found
	}
	
	/**
	 * This method returns the list of item in the format of an 2D array of objects.
	 * @return list of item in 2D array format
	 */
	public Object[][] stocksInObject() {
		Object[][] stocksInObject = new Object[Items.size()][7];
		Item item;
		for (int i = 0; i < Items.size(); i ++) {
			item = Items.get(i);
			stocksInObject[i][0] = new String(item.name());
			stocksInObject[i][1] = new Integer(item.quantity());
			stocksInObject[i][2] = new Integer(item.manufacturingCost());
			stocksInObject[i][3] = new Integer(item.sellPrice());
			stocksInObject[i][4] = new Integer(item.reorderPoint());
			stocksInObject[i][5] = new Integer(item.reorderAmount());
			if (item.temperature() == 25) {
				
			} else {
				stocksInObject[i][6] = new Integer(item.temperature());
			}
			
		}
		return stocksInObject;
	}
}
