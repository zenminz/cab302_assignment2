package GUI;

import java.awt.event.ActionEvent;  
import java.awt.event.ActionListener;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import CSV.CreateCSV;
import CSV.ManifestCSV;
import CSV.ParseCSV;
import CSV.SalesLogCSV;
import CSV.itemPropertiesCSV;
import Stock.Stocks;
import Stock.Updater;
import Store.Store;
import Delivery.*;
import Exception.CSVFormatException;
import Exception.DeliveryException;
import Exception.StockException;

/**
 * This class acts as the 'connection' passing values between each form
 * as well as operating the main action listeners
 * @author Peter
 *
 */

public class GUI {
	static Object[][] data;
	static boolean inventoryAccess;
	static boolean generateManifestAccess;
	static boolean loadManifestAccess;
	static boolean loadSalesAccess;
	static double costOrSell;
	static Store superMartStore;
	static Stocks itemStock = null;
	


	public static void main(String[] args) {
		//Main form initialisation
		superMartStore = new Store(100000.00, itemStock, "SuperMart");
		MainForm.mainForm();
		inventoryAccess = false;
		generateManifestAccess = false;
		loadManifestAccess = false;
		loadSalesAccess = false;
		
		
		//Item properties button
		MainForm.itemPropertiesBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser itemFc = new JFileChooser();
				itemFc.setCurrentDirectory(new java.io.File("."));
				itemFc.setDialogTitle("Select Item properties CSV to load");
				itemFc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				
				if (itemFc.showOpenDialog(MainForm.itemPropertiesBtn) == JFileChooser.APPROVE_OPTION) {
					ParseCSV itemCsv = null;
						try {
							itemCsv = new itemPropertiesCSV(itemFc.getSelectedFile());
							itemCsv.parseFile();
							Stocks itemStock = new Stocks(itemCsv.listOfItems());
							data = itemStock.stocksInObject();
							
							JOptionPane.showMessageDialog(MainForm.mainForm, "Inventory succesfully loaded");
							superMartStore = new Store(100000.00, itemStock, "SuperMart");
							InventoryForm.inventoryForm();
							MainForm.updateCapitalLbl(superMartStore.capital());
							inventoryAccess = true;
							generateManifestAccess = true;
							loadManifestAccess = true;
							loadSalesAccess = true;
							MainForm.itemPropertiesBtn.setEnabled(false);
						} catch (CSVFormatException e1) {
							JOptionPane.showMessageDialog(MainForm.mainForm, "The file is not in CSV format or is the incorrect file");
						} catch (DeliveryException e1) {
							JOptionPane.showMessageDialog(MainForm.mainForm, "Manifest contains item that does not exist in the inventory");
						} catch (StockException e1) {
							JOptionPane.showMessageDialog(MainForm.mainForm, "Insufficient item quantity to process the sale log");
						}				
				} else {
					JOptionPane.showMessageDialog(MainForm.mainForm, "You have not chosen a file");
				}
				
			}
		});
		//End item properties button
		
		//Inventory button 
		MainForm.inventoryBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (getInventoryAccess()) {
				MainForm.mainForm.setVisible(false);
				InventoryForm.inventoryFormVisible(true);
			} else {
				JOptionPane.showMessageDialog(MainForm.mainForm, "There is currently no inventory loaded");
			}
			}
		}); //End inventory button
		
		//Load manifest Button
		MainForm.loadManifestsBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (loadManifestAccess) {
					JFileChooser manifestFc = new JFileChooser();
					manifestFc.setCurrentDirectory(new java.io.File("."));
					manifestFc.setDialogTitle("Select manifest CSV to load");
					manifestFc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
					
					if (manifestFc.showOpenDialog(MainForm.loadManifestsBtn) == JFileChooser.APPROVE_OPTION) {
						ParseCSV manifestCsv = null;
							try {
								manifestCsv = new ManifestCSV(manifestFc.getSelectedFile(), superMartStore.inventory());
								manifestCsv.parseFile();
								Stocks manifestStock = new Stocks(manifestCsv.listOfItems());
								Updater updatingStock = new Updater(superMartStore.inventory(), manifestStock);
								updatingStock.updateStockAfterManifest();
								data = updatingStock.updatedInventory().stocksInObject();
								InventoryForm.updateTable(data);
								costOrSell = manifestCsv.listOfTrucks().totalCost();
								superMartStore.updateCapital(superMartStore.capital() - costOrSell);
								MainForm.updateCapitalLbl(superMartStore.capital());
											
								JOptionPane.showMessageDialog(MainForm.mainForm, "Your inventory has been updated");
							} catch (CSVFormatException e1) {
								JOptionPane.showMessageDialog(MainForm.mainForm, "The file is not in CSV format or is the incorrect file");
							} catch (DeliveryException e1) {
								JOptionPane.showMessageDialog(MainForm.mainForm, "Manifest contains item that does not exist in the inventory");
							} catch (StockException e1) {
								JOptionPane.showMessageDialog(MainForm.mainForm, "Insufficient item quantity to process the sale log");
							}


						

						
					} else {
						JOptionPane.showMessageDialog(MainForm.mainForm, "You have not chosen a file");
					}
					
				} else {
					JOptionPane.showMessageDialog(MainForm.mainForm, "There is currently no inventory loaded");
					
				}
				
				
			}
		});// End load manifest button
		
		
		//Export manifest button
		MainForm.exportManifestsBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (generateManifestAccess) {
					GenerateManifest manifestExport = new GenerateManifest(superMartStore.inventory());
					
					if (manifestExport.checkStock()) {
						manifestExport.generateListOfTrucks();
						costOrSell = manifestExport.totalCost();
						CreateCSV manifestCSVOut = new CreateCSV(manifestExport.trucks(), "manifestExport.csv");
						JFileChooser exportManifestFc = new JFileChooser();
						exportManifestFc.setCurrentDirectory(new java.io.File("."));
						exportManifestFc.setDialogTitle("Select folder to save manifest");
						exportManifestFc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
						
						if (exportManifestFc.showOpenDialog(MainForm.exportManifestsBtn) == JFileChooser.APPROVE_OPTION) {
							manifestCSVOut.generateCSV(exportManifestFc.getSelectedFile().getPath());
							JOptionPane.showMessageDialog(MainForm.mainForm, "Manifest CSV has successfully been created");
						} else {
							JOptionPane.showMessageDialog(MainForm.mainForm, "You have not selected a path to save your manifest");
						}				
					} else {
						JOptionPane.showMessageDialog(MainForm.mainForm, "Inventory does not currently need to be restocked");
					}
					
				} else { 
					JOptionPane.showMessageDialog(MainForm.mainForm, "There is currently no inventory loaded");
				}
			} 
		});
		
		//Sales log button
		MainForm.salesLogBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (loadSalesAccess) {
					JFileChooser salesFc = new JFileChooser();
					salesFc.setCurrentDirectory(new java.io.File("."));
					salesFc.setDialogTitle("Select sales CSV to load");
					salesFc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
					
					if (salesFc.showOpenDialog(MainForm.salesLogBtn) == JFileChooser.APPROVE_OPTION) {
						ParseCSV salesCsv = null;
							try {
								salesCsv = new SalesLogCSV(salesFc.getSelectedFile(), superMartStore.inventory());
								salesCsv.parseFile();
								
								Stocks saleStock = new Stocks(salesCsv.listOfItems());
								costOrSell = saleStock.totalSell();
								Updater updatingStock = new Updater(superMartStore.inventory(), saleStock);
								updatingStock.updateStockAfterSale();
								data = updatingStock.updatedInventory().stocksInObject();
								InventoryForm.updateTable(data);
								superMartStore.updateCapital(superMartStore.capital() + costOrSell);
								MainForm.updateCapitalLbl(superMartStore.capital());
								
								
								JOptionPane.showMessageDialog(MainForm.mainForm, "Sales have been loaded");
							} catch (CSVFormatException e1) {
								JOptionPane.showMessageDialog(MainForm.mainForm, "The file is not in CSV format or is the incorrect file");
							} catch (DeliveryException e1) {
								JOptionPane.showMessageDialog(MainForm.mainForm, "Manifest contains item that does not exist in the inventory");
							} catch (StockException e1) {
								JOptionPane.showMessageDialog(MainForm.mainForm, "Insufficient item quantity to process the sale log");
							}				
					} else {
						JOptionPane.showMessageDialog(MainForm.mainForm, "You have not chosen a file");
					}
				} else {
					JOptionPane.showMessageDialog(MainForm.mainForm, "There is currently no inventory loaded");
					
				}
			} 
		}); //End sales log


	}
	/**
	 *  This method returns the inventory data in Object[][] format.
	 * @return the updated data
	 */
	public static Object[][] getData() {
		return data;
	}
	
	/**
	 * This method decides if the inventory button is ready to be accessed.
	 * it returns a boolean result.
	 * @return true if the inventory button is ready to be accessed and false otherwise
	 */
	public static boolean getInventoryAccess() {
		return inventoryAccess;
	}
	

}
