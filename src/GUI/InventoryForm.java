package GUI;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * This class builds the inventory form. This form displays and updates the current inventory in tabular form.
 * When called, all values are intialised and resetted.
 * It contains a back button that redirects the user back to the mainform.
 * @author Peter
 *
 */
public class InventoryForm {
	//Intialising the Frame and Table
	static JFrame inventoryForm = new JFrame("Inventory");
	static DefaultTableModel tableModel;
	
	
	/**
	 * Method for initialising the Inventoryform
	 */
	public static void inventoryForm() {
		//Initialising the frame's properties
		inventoryForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		inventoryForm.setPreferredSize(new Dimension(900,550));
		inventoryForm.setLocation(new Point(550,150));
		inventoryForm.setResizable(true);
		
		//Iniitalising panel & properties
		JPanel inventoryPanel = new JPanel();
		inventoryPanel.setLayout(new BorderLayout());
		inventoryPanel.setBackground(Color.LIGHT_GRAY);
		inventoryForm.getContentPane().add(inventoryPanel);
		
		//The table's column names
		String[] columnNames = {"Name", "Quantity", "Cost($)", "Price ($)", "Reorder point", "Reorder quantity", "Temperature (c)"};


		
		//Initialising table properties (colour, size, header fonts, editability, table model)
		 JTable inventoryTbl = new JTable() {
			 private static final long serialVersionUID = -7031008862559936404L;

			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tableModel = new DefaultTableModel(GUI.getData(), columnNames);
		inventoryTbl.setModel(tableModel);
		inventoryTbl.setFillsViewportHeight(true);
		inventoryTbl.setFont(new Font("Calibri", Font.ITALIC, 20));
		inventoryTbl.setBackground(new Color(192, 192, 192));
		inventoryTbl.setForeground(Color.BLACK);
		inventoryTbl.setRowHeight(30);
		inventoryTbl.setPreferredScrollableViewportSize(new Dimension(450,63));

		JTableHeader inventoryTblHeader = inventoryTbl.getTableHeader();
		inventoryTblHeader.setBackground(SystemColor.textHighlight);
		inventoryTblHeader.setForeground(Color.WHITE);
		inventoryTblHeader.setFont(new Font("Calibri", Font.BOLD, 17));
		
		//Intialising scrollpane for the table to sit inside
		JScrollPane scrollPane = new JScrollPane(inventoryTbl);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		inventoryPanel.add(scrollPane);
		
		//Intialising back button to return to mainForm
		JButton backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inventoryForm.setVisible(false);
				MainForm.mainFormVisible(true);
			}
		});
		backBtn.setFont(new Font("Tahoma", Font.PLAIN, 17));
		inventoryPanel.add(backBtn, BorderLayout.PAGE_END);
		
		
		//Pack and visibility setting
		inventoryForm.pack();
		inventoryForm.setVisible(false);
		
		
	}
	
	/**
	 * This method sets the inventory form's visibility
	 * @param flag to indicate the form visibility
	 */
	public static void inventoryFormVisible(boolean flag) {
		inventoryForm.setVisible(flag);
	}
	
	/**
	 * This method updates the table every time new table data is generated
	 * @param newData
	 */
	public static void updateTable(Object[][] newData) {
		int rowCount = tableModel.getRowCount();
		for (int i = rowCount - 1; i >= 0; i--) {
			tableModel.removeRow(i);
		}
		
		for (int j = 0; j < newData.length; j++) {
			tableModel.addRow(newData[j]);
		}
		

		

	}
}
