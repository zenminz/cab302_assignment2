package Exception;

/**
 * This class creates the CSVFormationException subclass by extending
 * the exception class
 * @author Minh
 *
 */
public class CSVFormatException extends Exception{
	String message;
	
	public CSVFormatException(String message) {
		this.message = message;
	}
	public String message() {
		return message;
	}
}
