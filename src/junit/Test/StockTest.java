package junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import Stock.Item;
import Stock.Stocks;

public class StockTest {

	private static Stocks stocks;
	private static Item item;
	private static ArrayList<Item> items;
	private static String name = "test1";
	private static int manufacturingCost = 2;
	private static int sellPrice = 3;
	private static int reorderPoint = 200;
	private static int reorderAmount = 250;
	private static int temperature = 20;
	private static int quantity = 1;
	private static ArrayList<Item> actual;
	private static ArrayList<Item> expected;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		stocks = null;
		item = null;
		items = null; 
	
	}

	@Test
	public void testStockConstructor() {
		//stocks = new Stocks();
		items = new ArrayList<Item>();
		ArrayList<Object >actual = new ArrayList<Object>();
		ArrayList<Object >expected = new ArrayList<Object>();
		for (int i = 0; i < 3; i ++) {
			name = "test " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 20 + i;
			quantity = 1 + i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			items.add(item);
		}
		
		stocks = new Stocks(items);
		actual.add(Integer.toString(stocks.getSize()));
		expected.add(Integer.toString(items.size()));
		//actual = stocks.stocks();
		//expected = items;
		
		for (int i = 0; i < items.size(); i++) {
			actual.add(stocks.getItem(i).name());
			actual.add(stocks.getItem(i).manufacturingCost());
			actual.add(stocks.getItem(i).sellPrice());
			actual.add(stocks.getItem(i).reorderPoint());
			actual.add(stocks.getItem(i).reorderAmount());
			actual.add(stocks.getItem(i).temperature());
			actual.add(stocks.getItem(i).quantity());
			
			expected.add(items.get(i).name());
			expected.add(items.get(i).manufacturingCost());
			expected.add(items.get(i).sellPrice());
			expected.add(items.get(i).reorderPoint());
			expected.add(items.get(i).reorderAmount());
			expected.add(items.get(i).temperature());
			expected.add(items.get(i).quantity());
		}
		assertEquals(actual, expected);
		
	}
	
	
	@Test
	public void testStockConstructor2() {
		//stocks = new Stocks();
		Stocks items = new Stocks();
		ArrayList<Object >actual = new ArrayList<Object>();
		ArrayList<Object >expected = new ArrayList<Object>();
		for (int i = 0; i < 3; i ++) {
			name = "test " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 20 + i;
			quantity = 1 + i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			items.addItem(item);
		}
		
		stocks = new Stocks(items);
		actual.add(Integer.toString(stocks.getSize()));
		expected.add(Integer.toString(items.getSize()));
		//actual = stocks.stocks();
		//expected = items;
		
		for (int i = 0; i < items.getSize(); i++) {
			actual.add(stocks.getItem(i).name());
			actual.add(stocks.getItem(i).manufacturingCost());
			actual.add(stocks.getItem(i).sellPrice());
			actual.add(stocks.getItem(i).reorderPoint());
			actual.add(stocks.getItem(i).reorderAmount());
			actual.add(stocks.getItem(i).temperature());
			actual.add(stocks.getItem(i).quantity());
			
			expected.add(items.getItem(i).name());
			expected.add(items.getItem(i).manufacturingCost());
			expected.add(items.getItem(i).sellPrice());
			expected.add(items.getItem(i).reorderPoint());
			expected.add(items.getItem(i).reorderAmount());
			expected.add(items.getItem(i).temperature());
			expected.add(items.getItem(i).quantity());
		}
		assertEquals(actual, expected);
		
	}
	
	//This test is to test Stocks.addItem and Stocks.stocks();
	@Test
	public void testStock() {
		stocks = new Stocks();
		items = new ArrayList<Item>();
		for (int i = 0; i < 3; i ++) {
			name = "test " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 20 + i;
			quantity = 1 + i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			stocks.addItem(item);
			items.add(item);
		}
		actual = new ArrayList<Item>();
		expected = new ArrayList<Item>();
		
		actual = stocks.stocks();
		expected = items;
		
		assertEquals(actual, expected);
		
	}
	
	@Test
	public void testQuantityCostAndSell() {
		//stocks = new Stocks();
		items = new ArrayList<Item>();
		ArrayList<Object >actual = new ArrayList<Object>();
		ArrayList<Object >expected = new ArrayList<Object>();
		int totalQuantity = 0;
		double cost = 0;
		double sell = 0;
		
		for (int i = 0; i < 3; i ++) {
			name = "test " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 20 + i;
			quantity = 1 + i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			items.add(item);
		}
		
		stocks = new Stocks(items);
		actual.add(Integer.toString(stocks.getSize()));
		expected.add(Integer.toString(items.size()));
		//actual = stocks.stocks();
		//expected = items;
		
		for (int i = 0; i < items.size(); i++) {
			totalQuantity += items.get(i).quantity();
			cost += items.get(i).manufacturingCost() * items.get(i).quantity();
			sell += items.get(i).sellPrice() * items.get(i).quantity();
			
		}
		actual.add(stocks.totalQuantity());
		actual.add(stocks.totalCost());
		actual.add(stocks.totalSell());
		
		expected.add(totalQuantity);
		expected.add(cost);
		expected.add(sell);
		
		assertEquals(actual, expected);
	}
	
	@Test
	public void testRemoveAndPosition() {
		items = new ArrayList<Item>();
		ArrayList<Object >actual = new ArrayList<Object>();
		ArrayList<Object >expected = new ArrayList<Object>();
		
		for (int i = 0; i < 3; i ++) {
			name = "test " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 20 + i;
			quantity = 1 + i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			items.add(item);
		}
		
		stocks = new Stocks(items);
		
		actual.add(stocks.itemPosition("test 1"));
		actual.add(stocks.itemPosition("test 2"));
		actual.add(stocks.itemPosition("test 0"));
		
		expected.add(1);
		expected.add(2);
		expected.add(0);
		
		stocks.remove(1);
		actual.add(stocks.getSize());
		expected.add(2);
		actual.add(stocks.itemPosition("test 2"));
		expected.add(1);
		
		assertEquals(actual, expected);
		
	}
	
	@Test
	public void testObjects() {
		Object[][] actual = new Object[3][7];
		Object[][] expected = new Object[3][7];
		Object[][] itemsInObjectsList;
		stocks = new Stocks();
		
		for (int i = 0; i < 3; i ++) {
			name = "test " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 20 + i;
			quantity = 1 + i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			stocks.addItem(item);
			expected[i][0] = name;
			expected[i][1] = quantity;
			expected[i][2] = manufacturingCost;
			expected[i][3] = sellPrice;
			expected[i][4] = reorderPoint;
			expected[i][5] = reorderAmount;
			expected[i][6] = temperature;
		}
		actual = stocks.stocksInObject();
		assertEquals(actual, expected);
	} 

}
