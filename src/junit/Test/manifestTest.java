package junit.Test;

import static org.junit.Assert.*;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import Delivery.GenerateManifest;
import Delivery.Manifest;
import Delivery.Truck;
import Stock.Item;
import Stock.Stocks;

public class manifestTest {

	private static GenerateManifest generator;
	private static Stocks inventory;
	private static String name;
	private static int manufacturingCost;
	private static int sellPrice;
	private static int reorderPoint;
	private static int reorderAmount;
	private static int temperature;
	private static int quantity;
	private static Item item;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		generator = null;
		inventory = null;
	}

	static void generateInventory() {
		inventory = new Stocks();
		for (int i = 0; i < 11; i ++) {
			name = "test " + i;
			manufacturingCost = i;
			sellPrice = i;
			reorderPoint = i;
			reorderAmount = i;
			temperature = i;
			quantity = i + 1;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			inventory.addItem(item);
		}

	}
	
	//every item need to refill
	static void generateInventory2() {
		inventory = new Stocks();
		for (int i = 0; i < 11; i ++) {
			name = "test " + i;
			manufacturingCost = i;
			sellPrice = i;
			reorderPoint = 200;
			reorderAmount = 400;
			temperature = 25 - i * 3;
			if (temperature > 10) {
				temperature = 25;
			}
			if (temperature < -20) {
				temperature = -20;
			}
			
			quantity = 150;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			inventory.addItem(item);
		}
		

	}
	
	//only item without temp need to be refilled
	static void generateInventory3() {
		inventory = new Stocks();
		for (int i = 0; i < 11; i ++) {
			name = "test " + i;
			manufacturingCost = i;
			sellPrice = i;
			reorderPoint = 200;
			reorderAmount = 550;
			temperature = 25 - i * 3;
			if (temperature > 10) {
				temperature = 25;
			}
			if (temperature < -20) {
				temperature = -20;
			}
			
			if (temperature == 25) {
				quantity = 150;
			} else {
				quantity = 300;
			}
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			inventory.addItem(item);
		}

	}
	
	//only item with temp need to be refilled
	static void generateInventory4() {
		inventory = new Stocks();
		for (int i = 0; i < 11; i ++) {
			name = "test " + i;
			manufacturingCost = i;
			sellPrice = i;
			reorderPoint = 200;
			reorderAmount = 600;
			temperature = 25 - i * 3;
			if (temperature > 10) {
				temperature = 25;
			}
			if (temperature < -20) {
				temperature = -20;
			}
			
			if (temperature != 25) {
				quantity = 150;
			} else {
				quantity = 300;
			}
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			inventory.addItem(item);
		}

	}
	
	//check if stock need refill
	@Test
	public void testCheckStock() {
		generateInventory();
		generator = new GenerateManifest(inventory);
		assertFalse(generator.checkStock());
	}
	
	//check if stock need refill
	@Test
	public void testCheckStock2() {
		generateInventory2();
		generator = new GenerateManifest(inventory);
		assertTrue(generator.checkStock());
	}
	
	//test generate manifest (both item with temp and without temp)
	@Test
	public void testGenerateListOfTrucks() {
		ArrayList<String> actual = new ArrayList<String>();
		ArrayList<String> expected  = new ArrayList<String>();
		double actualCost = 0;
		double expectedCost = 0;
		DecimalFormat df = new DecimalFormat("#.####"); // format to 4 decimal places
		//boolean flag = false;
		
		generateInventory2();
		generator = new GenerateManifest(inventory);
		generator.checkStock();
		generator.generateListOfTrucks();
		
		Manifest trucks = generator.trucks();
		for (Truck truck : trucks.trucks()) {
			actual.add(truck.name());
		}
		actualCost = trucks.totalCost();
		actual.add(df.format(actualCost));
		
		expected.add("Refrigerated"); //temp = -5
		expected.add("Refrigerated"); //temp = 1
		expected.add("Ordinary");	//quantity = 1000;
		expected.add("Refrigerated"); //temp = 7
		expected.add("Ordinary"); // quantity = 1000;
		expectedCost += 900.0 + (200.0 * Math.pow(0.7,((double)-5/5)));
		expectedCost += 900.0 + (200.0 * Math.pow(0.7,((double)1/5)));
		expectedCost += 900.0 + (200.0 * Math.pow(0.7,((double)7/5)));
		expectedCost += (750.0 + 0.25 * 1000);
		expectedCost += (750.0 + 0.25 * 1000);
		for (Item item : inventory.stocks()) {
			expectedCost += item.manufacturingCost() * item.reorderAmount();
		}

		expected.add(df.format(expectedCost));
		
		//if (actual == expected && actualCost == expectedCost) {
		//	flag = true;
		//}
		
		assertEquals(expected, actual);
	}
	
	//only item without temp need to be refilled
	@Test
	public void testGenerateListOfTrucks2() {
		ArrayList<String> actual = new ArrayList<String>();
		ArrayList<String> expected  = new ArrayList<String>();
		double actualCost = 0;
		double expectedCost = 0;
		DecimalFormat df = new DecimalFormat("#.####"); // format to 4 decimal places
		//boolean flag = false;
		
		generateInventory3();
		generator = new GenerateManifest(inventory);
		generator.checkStock();
		generator.generateListOfTrucks();
		
		Manifest trucks = generator.trucks();
		for (Truck truck : trucks.trucks()) {
			actual.add(truck.name());
		}
		actualCost = trucks.totalCost();
		actual.add(df.format(actualCost));
		

		expected.add("Ordinary");	//quantity = 1000;
		expected.add("Ordinary"); // quantity = 1000;
		expected.add("Ordinary"); // quantity = 750;
		expectedCost += (750.0 + 0.25 * 1000);
		expectedCost += (750.0 + 0.25 * 1000);
		expectedCost += (750.0 + 0.25 * 750);
		for (Item item : inventory.stocks()) {
			if (item.temperature() == 25) {
				expectedCost += item.manufacturingCost() * item.reorderAmount();
			}

		}

		expected.add(df.format(expectedCost));
		
		//if (actual == expected && actualCost == expectedCost) {
		//	flag = true;
		//}
		
		assertEquals(expected, actual);
	}
	
	//only item with temp need to be refilled
	@Test
	public void testGenerateListOfTrucks3() {
		ArrayList<String> actual = new ArrayList<String>();
		ArrayList<String> expected  = new ArrayList<String>();
		double actualCost = 0;
		double expectedCost = 0;
		DecimalFormat df = new DecimalFormat("#.####"); // format to 4 decimal places
		//boolean flag = false;
		
		generateInventory4();
		generator = new GenerateManifest(inventory);
		generator.checkStock();
		generator.generateListOfTrucks();
		
		Manifest trucks = generator.trucks();
		for (Truck truck : trucks.trucks()) {
			actual.add(truck.name());
		}
		actualCost = trucks.totalCost();
		actual.add(df.format(actualCost));
		

		expected.add("Refrigerated"); //temp = -5
		expected.add("Refrigerated"); //temp = -2
		expected.add("Refrigerated"); //temp = 1
		expected.add("Refrigerated"); //temp = 7
		expected.add("Refrigerated"); //temp = 10
		expectedCost += 900.0 + (200.0 * Math.pow(0.7,((double)-5/5)));
		expectedCost += 900.0 + (200.0 * Math.pow(0.7,((double)-2/5)));
		expectedCost += 900.0 + (200.0 * Math.pow(0.7,((double)1/5)));
		expectedCost += 900.0 + (200.0 * Math.pow(0.7,((double)7/5)));
		expectedCost += 900.0 + (200.0 * Math.pow(0.7,((double)10/5)));
		for (Item item : inventory.stocks()) {
			if (item.temperature() != 25) {
				expectedCost += item.manufacturingCost() * item.reorderAmount();
			}

		}

		expected.add(df.format(expectedCost));
		
		//if (actual == expected && actualCost == expectedCost) {
		//	flag = true;
		//}
		
		assertEquals(expected, actual);
	}
}
