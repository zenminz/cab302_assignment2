package junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import Stock.Item;
import Stock.Stocks;
import Stock.Updater;

public class updaterTest {
	
	private static Stocks stocks;
	private static Stocks listOfItems;
	private static Updater updater;
	private static Item item;
	private static String name;
	private static ArrayList<Integer> actual;
	private static ArrayList<Integer> expected;
	private static int manufacturingCost;
	private static int sellPrice;
	private static int reorderPoint;
	private static int reorderAmount;
	private static int temperature;
	private static int quantity;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		stocks = null;
		item = null;
		listOfItems = null;
		updater = null;
		actual = null;
		expected = null;
		
	}

	@Test
	public void testManifest() {
		stocks = new Stocks();
		listOfItems = new Stocks();
		updater = new Updater(stocks, listOfItems);
		actual = new ArrayList<Integer>();
		expected = new ArrayList<Integer>();
		for (int i = 0; i < 3; i ++) {
			name = "test " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 20 + i;
			quantity = 1 + i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			stocks.addItem(item);
			listOfItems.addItem(item);
			expected.add(quantity * 2);
		}
		updater.updateStockAfterManifest();
		for (Item item : stocks.stocks()) {
			actual.add(item.quantity());
		}
		
		assertEquals(expected, actual);
	}
	
	@Test
	//try to update the stock with items those are not in the stock
	public void testManifest2() {
		stocks = new Stocks();
		listOfItems = new Stocks();
		updater = new Updater(stocks, listOfItems);
		actual = new ArrayList<Integer>();
		expected = new ArrayList<Integer>();
		
		for (int i = 0; i < 3; i ++) {
			name = "test " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 20 + i;
			quantity = 1 + i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			stocks.addItem(item);
			expected.add(quantity);
		}
		
		for (int i = 0; i < 3; i ++) {
			name = "test2 " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 20 + i;
			quantity = 1 + i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			listOfItems.addItem(item);
		}
		
		updater.updateStockAfterManifest();
		for (Item item : stocks.stocks()) {
			actual.add(item.quantity());
		}
		
		assertEquals(expected, actual);
	}
	
	@Test
	//Test update item after sale
	public void testSale() {
		stocks = new Stocks();
		listOfItems = new Stocks();
		updater = new Updater(stocks, listOfItems);
		actual = new ArrayList<Integer>();
		expected = new ArrayList<Integer>();
		
		for (int i = 0; i < 3; i ++) {
			name = "test " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 20 + i;
			quantity = 1 + i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			stocks.addItem(item);
			listOfItems.addItem(item);
			expected.add(0);
		}
		
		updater.updateStockAfterSale();
		for (Item item : stocks.stocks()) {
			actual.add(item.quantity());
		}
		
		assertEquals(expected, actual);
	
	}
	
	@Test
	//Test update item with items those are not in the stock after sale
	public void testSale2() {
		stocks = new Stocks();
		listOfItems = new Stocks();
		updater = new Updater(stocks, listOfItems);
		actual = new ArrayList<Integer>();
		expected = new ArrayList<Integer>();
		
		for (int i = 0; i < 3; i ++) {
			name = "test " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 20 + i;
			quantity = 1 + i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			stocks.addItem(item);
			expected.add(quantity);
		}
		
		for (int i = 0; i < 3; i ++) {
			name = "test2 " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 20 + i;
			quantity = 1 + i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			listOfItems.addItem(item);
		}
		
		updater.updateStockAfterSale();
		for (Item item : stocks.stocks()) {
			actual.add(item.quantity());
		}
		
		assertEquals(expected, actual);
	}
	
	//the exception when the item's quantity in the sale log is larger than 
	//the stock level is handled when the file is loaded in

}
