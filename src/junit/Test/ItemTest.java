package junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import Stock.Item;
import Stock.*;

public class ItemTest {

	private static Item item;
	private static String name = "test1";
	private static int manufacturingCost = 2;
	private static int sellPrice = 3;
	private static int reorderPoint = 200;
	private static int reorderAmount = 250;
	private static int temperature = 20;
	private static int quantity = 1;
	private static ArrayList<String> actual;
	private static ArrayList<String> expected;
	
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		item = null;
	}

	@Test
	public void testItem() {
		
		actual = new ArrayList<String>();
		expected = new ArrayList<String>();
		
		item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
		
		expected.add(name);
		expected.add(Integer.toString(manufacturingCost));
		expected.add(Integer.toString(sellPrice));
		expected.add(Integer.toString(reorderPoint));
		expected.add(Integer.toString(reorderAmount));
		expected.add(Integer.toString(temperature));
		expected.add(Integer.toString(quantity));
		
		actual.add(item.name());
		actual.add(Integer.toString(item.manufacturingCost()));
		actual.add(Integer.toString(item.sellPrice()));
		actual.add(Integer.toString(item.reorderPoint()));
		actual.add(Integer.toString(item.reorderAmount()));
		actual.add(Integer.toString(item.temperature()));
		actual.add(Integer.toString(item.quantity()));
		
		
		
		assertEquals(actual, expected);
	}
	
	@Test
	public void testUpdateQuantity() {
		quantity = 2;
		item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
		
		item.updateQuantity(10);
		
		int expected = 10;
		int actual = item.quantity();
		
		assertEquals(actual, expected); 
	}
}
