package junit.Test;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import Delivery.*;
import Stock.Item;
import Stock.Stocks;

public class truckTest {
	private static Truck noTempTruck;
	private static Truck tempTruck;
	private static Stocks noTempTruckCargo;
	private static Stocks tempTruckCargo;
	private static String name;
	private static int manufacturingCost;
	private static int sellPrice;
	private static int reorderPoint;
	private static int reorderAmount;
	private static int temperature;
	private static int quantity;
	private static Item item;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		noTempTruck = null;
		tempTruck = null;
		noTempTruckCargo = null;
		tempTruckCargo = null;
	}

	private static void setUpNoTempCargo() {
		noTempTruckCargo = new Stocks();
		for (int i = 0; i < 3; i ++) {
			name = "test " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = 25 + i;
			quantity = i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			noTempTruckCargo.addItem(item);
		}
	}
	
	private static void setUpTempCargo() {
		tempTruckCargo = new Stocks();
		for (int i = 0; i < 3; i ++) {
			name = "test " + i;
			manufacturingCost = 2 + i;
			sellPrice = 3 + i;
			reorderPoint = 200 + i;
			reorderAmount = 250 + i;
			temperature = -2 + i * i;
			quantity = i;
			item = new Item(name, manufacturingCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
			tempTruckCargo.addItem(item);
		}
		
	}
	@Test
	public void testNameNoTemptruck() {
		setUpTempCargo();
		tempTruck = new RefrigeratedTruck(tempTruckCargo);
		assertEquals(tempTruck.name(), "Refrigerated");
				
	}
	
	@Test
	public void testNameTemptruck() {
		setUpNoTempCargo();
		noTempTruck = new OrdinaryTruck(noTempTruckCargo);
		assertEquals(noTempTruck.name(), "Ordinary");
	}
	
	@Test
	public void updateCapacitytruck() {
		setUpTempCargo();
		tempTruck = new RefrigeratedTruck(tempTruckCargo);
		tempTruck.updateCapacity(10);
		
		setUpNoTempCargo();
		noTempTruck = new OrdinaryTruck(noTempTruckCargo);
		noTempTruck.updateCapacity(100);
		
		assertTrue("has to be 10 and 100", tempTruck.capacity() == 10 && noTempTruck.capacity() == 100);
	}
	
	@Test
	public void temperature() {
		
		setUpTempCargo();
		
		item = new Item("abc", 1, 1, 1, 1, -15, 2);
		tempTruckCargo.addItem(item);
		
		item = new Item("abc", 1, 1, 1, 1, -23, 2);
		tempTruckCargo.addItem(item);
		
		item = new Item("abc", 1, 1, 1, 1, -16, 2);
		tempTruckCargo.addItem(item);
		tempTruck = new RefrigeratedTruck(tempTruckCargo);
		
		//tempTruck.te();
		
		setUpNoTempCargo();
		noTempTruck = new OrdinaryTruck(noTempTruckCargo);
		noTempTruckCargo.addItem(item);
		
		assertTrue(tempTruck.temperature() == -20 && noTempTruck.temperature() == 0);
		
	}
	
	@Test
	public void testCost() {
		setUpTempCargo();
		tempTruck = new RefrigeratedTruck(tempTruckCargo);
		double expectedCost = 900.0 + (200.0 * Math.pow(0.7,((double)-2/5)));
		double actualCost = tempTruck.cost();
		
		setUpNoTempCargo();
		noTempTruck = new OrdinaryTruck(noTempTruckCargo);
		double expectedCost2 = (750.0 + 0.25 * 3);
		double actualCost2 = noTempTruck.cost();
		
		assertTrue(expectedCost == actualCost && expectedCost2 == actualCost2);
	}
	
	@Test
	public void testCargoQuantity() {
		boolean currentQuantity = true;
		boolean currentQuantity2 = true;
		boolean emptySpots = true;
		boolean emptySpots2 = true;
		
		setUpTempCargo();
		tempTruck = new RefrigeratedTruck(tempTruckCargo);
		tempTruck.truckCargo().getItem(0).updateQuantity(10);
		if (tempTruck.currentQuantity() != 10 + 3) {
			currentQuantity = false;
		}
		if (tempTruck.emptySpots() != 800 - 13) {
			emptySpots = false;
		}
		
		setUpNoTempCargo();
		noTempTruck = new OrdinaryTruck(noTempTruckCargo);
		noTempTruck.truckCargo().getItem(0).updateQuantity(11);
		noTempTruck.truckCargo().getItem(1).updateQuantity(12);
		noTempTruck.truckCargo().getItem(2).updateQuantity(16);
		if (noTempTruck.currentQuantity() != 11 + 12 + 16) {
			currentQuantity2 = false;
		}
		if (noTempTruck.emptySpots() != 1000 - 11 - 12 - 16) {
			emptySpots2 = false;
		}
		
		assertTrue(currentQuantity && currentQuantity2 && emptySpots && emptySpots2);
	}
}
