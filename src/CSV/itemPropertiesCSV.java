package CSV;

import java.io.IOException;
import java.util.ArrayList;
import Delivery.Manifest;
import Exception.CSVFormatException;
import Stock.Item;
import Stock.Stocks;
import java.io.*;

/**
 * This is subclass of the ParseCSV class. This subclass parses the data string from the item properties csv 
 * and puts those data in a Stocks object.
 * @author Minh
 *
 */
public class itemPropertiesCSV extends ParseCSV {
	ArrayList<String> textFromFile;
	Stocks itemPropertiesList;
	
	/**
	 * Constructor for initializing the file of a itemPropertiesCSV file, which is required to be parsed.
	 * This constructor also tries to read the file and stores the data of that file into 
	 * a string array
	 * 
	 * @param fileName of the file needs to be read and parsed
	 * @throws CSVFormatException when the file is not a csv file
	 */
	public itemPropertiesCSV(File fileName) throws CSVFormatException{
		super(fileName);

	}

	@Override
	public void parseFile() throws CSVFormatException {

		try {
			textFromFile = readFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		boolean correct = true;
		String[][] items_properties = new String[textFromFile.size()][6];
		String[] itemPropertiesToCheck;
		
		for (int i = 0; i < textFromFile.size(); i++) {
			itemPropertiesToCheck = textFromFile.get(i).split(",");
			if (itemPropertiesToCheck.length < 5 || itemPropertiesToCheck.length > 6) {
				correct = false;
				break;
			}
			
		}
		
		if (correct) {
			for (int i = 0; i < textFromFile.size(); i++) {
				items_properties[i] = textFromFile.get(i).split(","); 				
			}
			
			
			itemPropertiesList = new Stocks();
			Item item;
			String name;
			int manuCost;
			int sellPrice;
			int reorderPoint;
			int reorderAmount;
			int temperature;
			
			for (int i = 0; i < items_properties.length; i ++) {
				name = items_properties[i][0];
				manuCost = Integer.parseInt(items_properties[i][1]);
				sellPrice = Integer.parseInt(items_properties[i][2]);
				reorderPoint = Integer.parseInt(items_properties[i][3]);
				reorderAmount = Integer.parseInt(items_properties[i][4]);
					
				if (items_properties[i].length == 5) {
					temperature = 25;
					item = new Item(name, manuCost, sellPrice, reorderPoint, reorderAmount, temperature, 0);
				} else {
					temperature = Integer.parseInt(items_properties[i][5]);
					item = new Item(name, manuCost, sellPrice, reorderPoint, reorderAmount, temperature, 0);
				}
				itemPropertiesList.addItem(item);
			}
		} else {
			throw new CSVFormatException("not item properties");
		}
		
	}

	@Override
	public Stocks listOfItems() {
		return itemPropertiesList;
	}

	@Override
	public Manifest listOfTrucks() {
		return null;
	}

}
