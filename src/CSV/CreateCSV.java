package CSV;

import java.io.File; 
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import Delivery.Manifest;
import Delivery.Truck;
import Stock.Item;
/**
 * This CreateCSV class is creates an object which is used to create a csv file for the manifest
 * @author Minh
 *
 */
public class CreateCSV {
	private Manifest manifest;
	private String fileName;
	
	/**
	 * This constructor is used to create a new object of this class and
	 * to initialize the name of the csv file and list of trucks.
	 * @param manifest (list of trucks)
	 * @param fileName of the exported csv file
	 */
	public CreateCSV(Manifest manifest, String fileName) {
		this.manifest = manifest;
		this.fileName = fileName;
	}
	
	/**
	 * This method generates the csv file based on the manifest which was initialized from the
	 * constructor. The name of the file was initialised in in the constructor of this class
	 * @param filepath which the file is saved to 
	 */
	public void generateCSV(String filepath) {
		File file = new File(filepath + "\\" + fileName);
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		StringBuilder textToFile = new StringBuilder();
		for (Truck truck : manifest.trucks()) {
			textToFile.append(">");
			textToFile.append(truck.name());
			textToFile.append(",");
			textToFile.append("\n");
			for (Item item : truck.truckCargo().stocks()) {
				textToFile.append(item.name());
				textToFile.append(",");
				textToFile.append(item.quantity());
				textToFile.append("\n");
			}
		}
		
		writer.write(textToFile.toString());
		writer.close();		
	} 
}
