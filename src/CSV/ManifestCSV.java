package CSV;

import java.io.IOException; 
import java.util.ArrayList;
import java.io.*;
import Delivery.Manifest;
import Delivery.OrdinaryTruck;
import Delivery.RefrigeratedTruck;
import Delivery.Truck;
import Exception.CSVFormatException;
import Exception.DeliveryException;
import Stock.Item;
import Stock.Stocks;

/**
 * This is subclass of the ParseCSV class. This subclass parses the data string from the manifest csv 
 * and puts those data in a Manifest object.
 * @author Minh
 *
 */
public class ManifestCSV extends ParseCSV {
	
	ArrayList<String> textFromFile;
	Stocks manifest;
	Manifest trucks;
	/**
	 * Constructor for initializing the file of a manifestCSV file, which needs to be parsed.
	 * This constructor also tries to read the file and stores the data of that file into 
	 * a string array 
	 * @param fileName which needs to be parsed
	 * @param currentInventory which is the current list of items in the store
	 * @throws CSVFormatException when the file is not a csv file
	 */
	public ManifestCSV(File fileName, Stocks currentInventory) throws CSVFormatException {
		super(fileName, currentInventory);
		try {
			textFromFile = readFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * This method sorts the item in the manifest list in ascending order based on 
	 * the quantities of the items in the list
	 * @param cargo
	 */
	static void insertionSort(Stocks cargo) {
		for (int i = 1; i < cargo.getSize(); i ++) {
			int value = cargo.getItem(i).quantity();
			Item value2 = cargo.getItem(i);
			int test_slot = i - 1;
			while (test_slot > -1 && cargo.getItem(test_slot).quantity() > value) {
				cargo.stocks().set(test_slot + 1, cargo.getItem(test_slot));
				test_slot = test_slot - 1;
			}
			cargo.stocks().set(test_slot + 1, value2);
		}
	}
	
	@Override
	public void parseFile() throws CSVFormatException, DeliveryException {
		
		/*
		 * Gets the items in the manifest and stores them in a Stocks object
		 */
		String[][] manifestString = new String[textFromFile.size()][6];
		boolean correct = true;
		boolean manifestCorrect = true;
		manifestString[0] = textFromFile.get(0).split(",");
		if (!manifestString[0][0].contains(">Ordinary") && !manifestString[0][0].contains(">Refrigerated")) {
			correct = false;
		}
		
		
		if (correct) {
			manifest = new Stocks();
			Item item;
			String name = null;
			int manuCost = 0;
			int sellPrice = 0;
			int reorderPoint = 0;
			int reorderAmount = 0;
			int temperature = 0;
			int quantity = 0;
			
			for (int i = 0; i < manifestString.length; i ++) {
				manifestString[i] = textFromFile.get(i).split(",");
				if (manifestString[i].length == 2) {
					name = manifestString[i][0];
					quantity = Integer.parseInt(manifestString[i][1]);
					
					for (int k = 0; k < currentInventory.getSize(); k++) {
						if (name.equals(currentInventory.getItem(k).name())) {
							manuCost = currentInventory.getItem(k).manufacturingCost();
							sellPrice = currentInventory.getItem(k).sellPrice();
							reorderPoint = currentInventory.getItem(k).reorderPoint();
							reorderAmount = currentInventory.getItem(k).reorderAmount();
							temperature = currentInventory.getItem(k).temperature();
							
							break;
						}									
						
					}
					item = new Item(name, manuCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
					manifest.addItem(item); 
				}
			
			}
						
			int counter = 0;
			for (int i = 0; i < manifest.getSize(); i ++) {
				Item item1 = manifest.getItem(i);
				for (int j = i + 1; j < manifest.getSize(); j ++) {
					
					Item item2 = manifest.getItem(j);
					if (item2.name().equals(item1.name()) && (item2.quantity() != 0)) {
						item1.updateQuantity(item1.quantity() + item2.quantity());
						item2.updateQuantity(0);

						
					}
				}
				if (item1.quantity() == 0) {
					counter += 1;
				}
			}
			insertionSort(manifest);
			
			for (int i = 0; i < counter; i++) {
				manifest.remove(0);
			}
			
			for (int i = 0; i < manifest.getSize(); i ++) {
				name = manifest.getItem(i).name();
				for (int k = 0; k < currentInventory.getSize(); k++) {
					if (!name.equals(currentInventory.getItem(k).name())) {
						manifestCorrect = false;
						//break;
					} else {
						manifestCorrect = true;
						break;
					}
					
				}
				if (!manifestCorrect) {
					break;
				}
			}
			
			if (manifestCorrect) {
			} else {
				throw new DeliveryException();
			}
			
			trucks = new Manifest();
			Truck truck = null;
			Stocks cargo = null;
			for (int i = 0; i < textFromFile.size(); i++) {
				manifestString[i] = textFromFile.get(i).split(",");
				if (manifestString[i][0].contains(">Refrigerated")) {
					if (cargo != null && truck != null) {
						trucks.addTruck(truck);
					}
					cargo = new Stocks();
					truck = new RefrigeratedTruck(cargo);
				} else if (manifestString[i][0].contains(">Ordinary")) {
					if (cargo != null && truck != null) {
						trucks.addTruck(truck);
					}
					cargo = new Stocks();
					truck = new OrdinaryTruck(cargo);
				} else {
					name = manifestString[i][0];
					quantity = Integer.parseInt(manifestString[i][1]);
					for (int k = 0; k < currentInventory.getSize(); k++) {
						if (name.equals(currentInventory.getItem(k).name())) {
							manuCost = currentInventory.getItem(k).manufacturingCost();
							sellPrice = currentInventory.getItem(k).sellPrice();
							reorderPoint = currentInventory.getItem(k).reorderPoint();
							reorderAmount = currentInventory.getItem(k).reorderAmount();
							temperature = currentInventory.getItem(k).temperature();
							break;
						}									
						
					}
					item = new Item(name, manuCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
					cargo.addItem(item);
					
				}
			}
			
			
			if (cargo != null && truck != null) {
				trucks.addTruck(truck);
			}
			
		} else {
			throw new CSVFormatException("not manifest");
		}
		

	}
	
	/**
	 * This method calculate the total cost of trucks and their cargo and returns that total cost
	 * @return total cost of trucks and their cargo
	 */
	public double totalCost() {
		double totalCost = 0;
		for (Truck truck : trucks.trucks()) {
			totalCost += truck.cost() + truck.truckCargo().totalCost(); 
		}
		
		return totalCost;
	}
	
	@Override
	public Stocks listOfItems() {
		return manifest;
	}

	@Override
	public Manifest listOfTrucks() {
		return trucks;
	}
	

}
