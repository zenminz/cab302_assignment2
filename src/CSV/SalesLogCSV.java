package CSV;

import java.io.IOException; 
import java.util.ArrayList;
import java.io.*;
import Delivery.Manifest;
import Exception.CSVFormatException;
import Exception.StockException;
import Stock.Item;
import Stock.Stocks;
/**
 * This is a subclass of the ParseCSV class. This subclass parses the data string from the sale log csv 
 * and places the data in a Stocks object.
 * @author Minh
 *
 */
public class SalesLogCSV extends ParseCSV {

	ArrayList<String> textFromFile;
	Stocks saleLog;
	
	/**
	 * Constructor for initializing the file name of a SalesLogCSV file, which needs to be parsed.
	 * This constructor also tries to read the file and stores the data of that file into 
	 * a string array
	 * 
	 * @param fileName of the file needs to be read and parsed
	 * @param currentInventory which is the current list of items of the store
	 * @throws CSVFormatException when the file is not a csv file
	 */
	public SalesLogCSV(File fileName, Stocks currentInventory) throws CSVFormatException {
		super(fileName, currentInventory);
		try {
			textFromFile = readFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	@Override
	public void parseFile() throws CSVFormatException, StockException{

		String[][] saleLogSting = new String[textFromFile.size()][3];
		boolean correct = true; //set a flag to check if the lines failed the specification
		boolean saleSmallerThanStock = true; //set a flag to check if the item's quantity in the sale log is smaller than item;s qyuantity in stock
		for (int i = 0; i < textFromFile.size(); i++) {
			saleLogSting[i] = textFromFile.get(i).split(",");
			if (saleLogSting[i].length != 2) {
				correct = false;
				break;
			}
		}
		
		if (correct) {
			for (int i = 0; i < textFromFile.size(); i++) {
				saleLogSting[i] = textFromFile.get(i).split(",");
			}
			
			saleLog = new Stocks();
			Item item;
			String name;
			int manuCost = 0;
			int sellPrice = 0;
			int reorderPoint = 0;
			int reorderAmount = 0;
			int temperature = 0;
			int quantity = 0;

			
			for (int i = 0; i < saleLogSting.length; i ++) {
				name = saleLogSting[i][0];
				quantity = Integer.parseInt(saleLogSting[i][1]);
				
				for (int k = 0; k < currentInventory.getSize(); k++) {
					if (name.equals(currentInventory.getItem(k).name())) {
						if (quantity > currentInventory.getItem(k).quantity()) {
							saleSmallerThanStock = false;
							break;
						}
						manuCost = currentInventory.getItem(k).manufacturingCost();
						sellPrice = currentInventory.getItem(k).sellPrice();
						reorderPoint = currentInventory.getItem(k).reorderPoint();
						reorderAmount = currentInventory.getItem(k).reorderAmount();
						temperature = currentInventory.getItem(k).temperature();
						
						break;
					}									
					
				}
				if (!saleSmallerThanStock) {
					throw new StockException();
					
				} else {
					item = new Item(name, manuCost, sellPrice, reorderPoint, reorderAmount, temperature, quantity);
					saleLog.addItem(item); 
				}
			
			}
		} else {
			throw new CSVFormatException("not sale log");
		}
		
		

		
	}

	@Override
	public Stocks listOfItems() {
		return saleLog;
	}

	@Override
	public Manifest listOfTrucks() {
		return null;
	}
	

}
