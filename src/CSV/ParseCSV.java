package CSV;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import Delivery.Manifest;
import Exception.CSVFormatException;
import Exception.DeliveryException;
import Exception.StockException;
import Stock.Stocks;
import java.io.*;

/**
 * This abstract class contains common methods for parsing a csv file. There are many type of csv files 
 * for different purposes in this case: item properties csv, sales log csv and manifest csv. Each of them
 * needs to be parsed and stored in different ways.
 * 
 * @author Minh
 *
 */
public abstract class ParseCSV {
	File fileName;
	Stocks listItems;
	Stocks currentInventory;
	ArrayList<String> textFromFile;
	
	/**
	 * Another constructor which is used to create a new object of this class
	 * and to initialize the input file. This constructor also checks the file extension 
	 * to see if its a csv file
	 * @param fileName the files need to be parsed
	 * @throws CSVFormatException when the file is a csv file
	 */
	public ParseCSV(File fileName) throws CSVFormatException {
		String name = fileName.getName();
		if (fileName.getName().matches("(.*).csv")) {
			this.fileName = fileName;
		} else {
			
			throw new CSVFormatException("not in .csv");
		}

		
	}
	
	/**
	 * Another constructor which is used to create a new object of this class
	 * and to initialize the input file. This constructor also check the file extension 
	 * to see if its a csv file
	 * @param fileName the file needs to be parsed
	 * @param currentInventory (current list of items of the store)
	 * @throws CSVFormatException when the file is not a csv file
	 */
	public ParseCSV(File fileName, Stocks currentInventory) throws CSVFormatException {
		if (fileName.getName().matches("(.*).csv")) {
			
			this.fileName = fileName;
			this.currentInventory = new Stocks(currentInventory);
		} else {
			throw new CSVFormatException("not in .csv");
		}

	}
	
	/**
	 * This method reads the file and stores every line from inside that file into an array. This method will 
	 * return that array.
	 * 
	 * @return the array which contains the data in the file
	 * 
	 * @throws IOException when the file is not found
	 */
	public ArrayList<String> readFile() throws IOException {
		//String fileName = "item_properties.csv";
		FileReader reader = new FileReader(fileName);
		BufferedReader bufferedReader = new BufferedReader(reader);
		String line;// = bufferedReader.readLine();
		//String[] lines2;
		
		textFromFile = new ArrayList<String>();
		
		while ((line = bufferedReader.readLine()) != null) {
			textFromFile.add(line);
			//lines2 = line.split(",");
		}

		bufferedReader.close();
		return textFromFile;
	}
	
	/**
	 * This abstract method parses the file and places the data in its appropriate data format.
	 * Each CSV has their own data type structure.
	 * @throws CSVFormatException when the file is not in correct format (item_properties, saleLog and manifest. 
	 * Each of those type of csv has their own format)
	 * @throws DeliveryException when manifest contain item that is not in the store's inventory
	 * @throws StockException when Insufficient item quantity to process the sale log.
	 */
	public abstract void parseFile() throws CSVFormatException, DeliveryException, StockException;
	
	/**
	 * This abstract method returns the list of item as a Stocks object after parsing.
	 * @return list of items as a Stocks object after parsing
	 */
	public abstract Stocks listOfItems();
	
	/**
	 * This abstract method returns the list of shipping trucks in the manifest.
	 * THis method is only needed for ManifestCSV subclass
	 * @return list of shipping trucks in the manifest.
	 */
	public abstract Manifest listOfTrucks();
}
