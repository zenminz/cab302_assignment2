package Delivery;

import Stock.Stocks;

/**
 * This Refrigerated Truck class is a subclass of Truck, which can be use to create
 * a new truck object
 * @author Minh
 *
 */
public class RefrigeratedTruck extends Truck {
	
	private double cost;
	private int temperature;
	private int capacity;
	private String name;
	
	/**
	 * Constructor for creating a new RefrigeratedTruck and initializing its properties
	 * such as: truck's cargo, truck's temperature, truck's capacity, truck'name and 
	 * truck's cost.
	 * @param truckCargo which the cargo of the truck
	 */
	public RefrigeratedTruck(Stocks truckCargo) {
		super(truckCargo);
		setTemperature();
		this.capacity = 800;
		this.name = "Refrigerated";
		this.cost = cost();
	}
	
	/**
	 * This method sets the cost and returns the cost for the Refrigerated truck.
	 * The cost of the Refrigerated truck is different from the cost for Ordinary truck.
	 * The cost for this truck is inversely proportional to the lowest temperature
	 * of an item in the truck's cargo. 
	 */
	@Override
	public double cost() {
		setTemperature();
		cost = 900.0 + (200.0 * Math.pow(0.7,((double)temperature/5)));
		return cost;
	}

	@Override
	public void updateCapacity(int capacity) {
		this.capacity = capacity;
		
	}
	
	/**
	 * This method sets the temperature of the truck to the temperature of the item which has the lowest
	 * temperature in the cargo
	 */
	public void setTemperature() {
		int sizeOfTruckCargo = truckCargo.getSize();
		int smallest = 99;
		for (int i = 0; i < sizeOfTruckCargo - 1; i++) {
			if (truckCargo.getItem(i).temperature() < smallest) {
				smallest = truckCargo.getItem(i).temperature();
			}
		}
		
		if (smallest < -20) {
			smallest = -20;
		}
		
		if (smallest > 10) {
			smallest = 10;
		}
		temperature = smallest;
	}
	
	/**
	 * This method return the current capacity for the Refrigerated truck.
	 * The max capacity for a Refrigerated truck is 800.
	 */
	@Override
	public int capacity() {
		return capacity;
	}
	
	/**
	 * THis method returns the name of this truck, which is Refrigerated truck
	 */
	@Override
	public String name() {
		return name;
	}

	@Override
	public int temperature() {
		setTemperature();
		return temperature;
	}
}
