package Delivery;

import Stock.Item; 
import Stock.Stocks;

/**
 * This is the abstract class for the two truck types: Refrigerated Truck and
 * Ordinary Truck. This abstract class contains common properties for the
 * two other subclasses such as: truck's cargo, truck's name, truck's cost,
 *  truck's capacity, truck's temperature
 *  
 * @author Minh
 *
 */
public abstract class Truck {
	Stocks truckCargo;
	
	/**
	 * Constructor for creating and initializing a truck with a cargo.
	 * 
	 * @param truckCargo which is the cargo of the truck
	 */
	public Truck(Stocks truckCargo) {
		this.truckCargo = truckCargo;	
	}	
	
	/**
	 * This abstract method sets a cost for the truck. Each truck has its' own cost.
	 * This abstract method also returns the cost belong to that truck.
	 * 
	 * @return cost belong to that truck
	 */
	public abstract double cost();
	
	/**
	 * This abstract method update the truck's capacity to the same value as the input
	 * 
	 * @param capacity which is the new capacity
	 */
	public abstract void updateCapacity(int capacity);
	
	/**
	 * This abstract method returns the name of the truck.
	 * 
	 * @return name of the truck
	 */
	public abstract String name();
	
	/**
	 * This abstract method returns the capacity of the truck
	 * 
	 * @return capacity of truck
	 */
	public abstract int capacity();
	
	/**
	 * This abstract method returns the temperature of the truck. Refrigerated truck
	 * needs to maintains a safe temperature for its' cargo.
	 * 
	 * @return temperature of the truck
	 */
	public abstract int temperature();
	
	/**
	 * This abstract method returns the truck's cargo.
	 * 
	 * @return truck's cargo
	 */
	public Stocks truckCargo() {
		return truckCargo;
	}
	
	/**
	 * This method calculate the total quantity of the items in the truck's cargo and returns
	 * that total quantity
	 * @return total quantity of items in the truck's cargo
	 */
	public int currentQuantity() {
		int currentQuantity = 0;
		for (Item item : truckCargo.stocks()) {
			currentQuantity += item.quantity();
		}
		return currentQuantity;
	}
	
	/**
	 * This method calculate the number of empty Spots of the truck's cargo
	 * @return total number of empty spots
	 */
	public int emptySpots() {
		int emptySpots = 0;
		emptySpots = capacity() - currentQuantity();
		return emptySpots;
	}
}
