package Delivery;

import java.util.ArrayList;

/**
 * This class contains list of trucks and information that belongs to those trucks such as: their cargo, and
 * total cost.
 * 
 * @author Minh
 *
 */
public class Manifest {
	private ArrayList<Truck> trucks;
	
	public Manifest() {
		trucks = new ArrayList<Truck>();
	}
	
	/**
	 * This method adds a truck to the list of trucks
	 * 
	 * @param truck which is the trucks need to be added to the list
	 */
	public void addTruck (Truck truck) {
		trucks.add(truck);
	}
	
	/**
	 * This method returns the list of trucks
	 * 
	 * @return list of trucks
	 */
	public ArrayList<Truck> trucks() {
		return trucks;
	}
	
	/**
	 * This method calculates the total cost of trucks and their cargo and return that cost
	 * 
	 * @return the total cost of trucks and their cargo
	 */
	public double totalCost() {
		double totalCost = 0;
		for (Truck truck : trucks) {
			totalCost += truck.cost() + truck.truckCargo().totalCost(); 
		}
		
		return totalCost;
	}

}
