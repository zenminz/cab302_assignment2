package Delivery;

import Stock.Item;
import Stock.Stocks;

/**
 * This class can be used to generate a list of items that are needed
 * to be refilled. This class also generates a list of suitable trucks to 
 * deliver the items and a suitable cargo to optimize the cost.
 * 
 * @author Minh
 *
 */
public class GenerateManifest {
	
	private Manifest trucks;
	private Stocks currentStock;
	private static Stocks manifest;
	
	/**
	 * Constructor for creating new manifest object, which takes a current store's inventory
	 * as input. THis constructor also initialize the variables suck as: an empty arayList
	 * to store trucks, the current stock and a empty manifest to store the items those
	 * needed to be refilled.
	 * @param currentStock which is the current stock level
	 */
	public GenerateManifest (Stocks currentStock) {
		trucks = new Manifest();
		this.currentStock = new Stocks(currentStock);
		manifest = new Stocks();
	}
	
	/**
	 * This method check the current stock to see if any items need to be refilled by 
	 * comparing those items' quantity with their reorder point. Every item which has a
	 * quantity smaller than its reorder point is added to the manifest list.
	 * This method also generates the list of items which need to be refilled.
	 * 
	 * @return needRefill if the items need to be refilled and !needRefill if the items don't 
	 * need to be refilled
	 */
	public boolean checkStock () {
		boolean needRefill = false;
		for (Item item : currentStock.stocks()) {
			if (item.quantity() <= item.reorderPoint()) {
				item.updateQuantity(item.reorderAmount());
				manifest.addItem(item);
				needRefill = true;
			}
		}
		
		return needRefill;
	}
	
	/**
	 * This method returns the list of items of those needed to be refilled
	 * 
	 * @return list of items needed to be refilled
	 */
	public Stocks manifest() {
		return manifest;
	}
	
	/**
	 * This method sort the items in the truck's cargo in ascending order based on
	 * the item's temperature.
	 * 
	 * @param cargo
	 */
	static void insertionSort(Stocks cargo) {
		for (int i = 1; i < cargo.getSize(); i ++) {
			int value = cargo.getItem(i).temperature();
			Item value2 = cargo.getItem(i);
			int test_slot = i - 1;
			while (test_slot > -1 && cargo.getItem(test_slot).temperature() > value) {
				cargo.stocks().set(test_slot + 1, cargo.getItem(test_slot));
				test_slot = test_slot - 1;
			}
			cargo.stocks().set(test_slot + 1, value2);
		}
	}
	
	/**
	 * checkStock needs to be called first to initialise the list of items which need
	 * to be refilled.
	 * This method generates a list of trucks with their required cargo. 
	 * Since the price of the delivery trucks are dependent on the temperature
	 * of the item, the manifest list was ordered in ascending order. 
	 * This way all the items with a lower temperature were able to be grouped 
	 * within the same truck.
	 */
	public void generateListOfTrucks() {
		Stocks tempCargo = new Stocks(); //cargo to store items with temperature
		Stocks noTempCargo = new Stocks(); // cargo to store items without temperature
		Stocks refilledItems = new Stocks(manifest); // clone the manifest 
		insertionSort(refilledItems); // sort the list based on item's temperature
		
		Truck tempTruck = new RefrigeratedTruck(tempCargo); // truck to store items with temperature
		Truck noTempTruck = new OrdinaryTruck(noTempCargo); // truck to store items without temperature
		
		boolean repeat = true;
		Item clonedItem;
		int newCapacity = 0;
		for (Item item : refilledItems.stocks()) {
			while (item.quantity() != 0) {
				//Item with temp
				if (item.temperature() != 25) {
					/*
					 * Check if tempTruck has any emptySpot
					 * if Yes do another check 
					 * Else add the current tempTruck to the trucks list and create new truck and new stock
					 */
					if (tempTruck.emptySpots() > 0) {
						/*
						 * check if item's quantity is larger than the empty spot
						 * If it is put some quantity of item in those empty spot
						 * and update the quantity of that item in the item list
						 * else put that item in the truck's cargo, update the truck's
						 * empty spot and update the quantity of that item to 0 on the item list
						 */
						if (item.quantity() > tempTruck.emptySpots()) {
							newCapacity = item.quantity() - tempTruck.emptySpots();
							item.updateQuantity(item.quantity() - newCapacity);
							tempCargo.addItem(new Item(item));
							item.updateQuantity(newCapacity);
						} else {
							newCapacity = tempTruck.currentQuantity() - item.quantity();
							clonedItem = new Item(item);
							tempCargo.addItem(new Item(item));
							item.updateQuantity(0);
						}
					} else {
    					tempTruck = new RefrigeratedTruck(tempCargo);
    					trucks.addTruck(tempTruck);
    					tempCargo = new Stocks();  					
    					tempTruck = new RefrigeratedTruck(tempCargo);
					}
				//Item without temperature
				} else if (item.temperature() == 25) {
					/*
					 * Check if the temptruck has any emptySpots
					 * If yes squeeze the item without temp in to minimize the cost
					 */
					if (tempTruck.emptySpots() > 0 && tempTruck.currentQuantity() > 0) {
						if (item.quantity() > tempTruck.emptySpots()) {
							newCapacity = item.quantity() - tempTruck.emptySpots();
							item.updateQuantity(item.quantity() - newCapacity);
							tempCargo.addItem(new Item(item));
							item.updateQuantity(newCapacity);
						} else {
							newCapacity = tempTruck.currentQuantity() - item.quantity();
							clonedItem = new Item(item);
							tempCargo.addItem(new Item(item));
							item.updateQuantity(0);
						}
					}
					/*
					 * Check if noTempTruck has any emptySpot
					 * if Yes do another check 
					 * Else add the current noTempTruck to the trucks list and create new truck and new stock
					 */
					if (noTempTruck.emptySpots() > 0) {
						/*
						 * check if item's quantity is larger than the empty spot
						 * If it is put some quantity of item in those empty spot
						 * and update the quantity of that item in the item list
						 * else put that item in the truck's cargo, update the truck's
						 * empty spot and update the quantity of that item to 0 on the item list
						 */
						if (item.quantity() > noTempTruck.emptySpots()) {
							newCapacity = item.quantity() - noTempTruck.emptySpots();
							item.updateQuantity(item.quantity() - newCapacity);
							noTempCargo.addItem(new Item(item));
							item.updateQuantity(newCapacity);
						} else {
							newCapacity = noTempTruck.currentQuantity() - item.quantity();
							clonedItem = new Item(item);
							noTempCargo.addItem(new Item(item));
							item.updateQuantity(0);
						}
					} else {
						noTempTruck = new OrdinaryTruck(noTempCargo);
    					trucks.addTruck(noTempTruck);
    					noTempCargo = new Stocks();  					
    					noTempTruck = new OrdinaryTruck(noTempCargo);
					}
					
				}
			}
		}
		
        //add last truck to the list
        if (tempCargo.getSize() > 0) {
        	tempTruck = new RefrigeratedTruck(tempCargo);
        	trucks.addTruck(tempTruck);
        }
		
		
		//add last truck to the list
        if (noTempCargo.getSize() > 0) {
        	noTempTruck = new OrdinaryTruck(noTempCargo); 
        	trucks.addTruck(noTempTruck);
        }
		
	}
	
	/**
	 * This method calculate the total cost of the trucks and their cargo and returns that cost
	 * 
	 * @return the total cost of trucks and their cargo
	 */
	public double totalCost() {
		double totalCost = 0;
		for (Truck truck : trucks.trucks()) {
			totalCost += truck.cost() + truck.truckCargo().totalCost(); 
		}
		
		return totalCost;
	}
	
	/**
	 * This method return the list of trucks for shipping with their cargo.
	 * 
	 * @return list of trucks for shipping.
	 */
	public Manifest trucks() {
		return trucks;
	}
}
