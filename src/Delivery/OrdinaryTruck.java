package Delivery;

import Stock.Stocks;

/**
 * This Ordinary Truck class is a subclass of Truck, which can be use to create
 * a new truck object
 * @author Minh
 *
 */
public class OrdinaryTruck extends Truck {
	
	//Declare variables
	private double cost;
	private int capacity;
	private String name;
	
	/**
	 * Constructor for creating a new Ordinary and initializing its properties
	 * such as: truck's cargo, truck's temperature, truck's capacity, truck'name and 
	 * truck's cost.
	 * @param truckCargo which the cargo of the truck
	 */
	public OrdinaryTruck(Stocks truckCargo) {
		super(truckCargo);
		this.capacity = 1000;
		this.name = "Ordinary";
		this.cost = 0;
	}

	/**
	 * This method sets the cost and returns the cost for the Ordinary truck.
	 * The cost for the Ordinary truck is different from the cost of the Refrigerated truck.
	 * The cost for this truck depends on the total quantity of the truck's cargo.
	 */
	@Override
	public double cost() {
		double q = truckCargo.totalQuantity();
		cost = (750.0 + 0.25 * q);
		return cost;
	}

	@Override
	public void updateCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	@Override
	public int capacity() {
		return capacity;
	}

	@Override
	public String name() {
		return name;
	}
	
	/**
	 * This method doesn't need to return anything because this truck 
	 * doesn't need to maintain its' cargo at any temperature levels.
	 */
	@Override
	public int temperature() {
		return 0;
	}
}
