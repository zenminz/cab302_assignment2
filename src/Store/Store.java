package Store;

import Stock.*;
/**
 * This Store class can be used to create a Store object, which contains information of a store such its name, capital, and inventory.
 * @author Minh
 *
 */
public class Store {
	private double capital;
	private Stocks inventory;
	private String name;
	
	/**
	 * This constructor is used to construct a store object, and also to initialize that store's properties.
	 * @param capital of the store
	 * @param inventory of the store
	 * @param name of the store
	 */
	public Store (double capital, Stocks inventory, String name) {
		this.capital = capital;
		this.inventory = inventory;
		this.name = name;	
	}
	
	/**
	 * This method return the current capital of the store. 
	 * @return current capital of the store
	 */
	public double capital () {
		return capital;
	}
	
	/**
	 * This method returns the stock (inventory) of the store.
	 * @return stock/inventory of the store
	 */
	public Stocks inventory () {
		return inventory;
	}
	
	/**
	 * This method returns the name of the store
	 * @return name of the store
	 */
	public String name () {
		return name;
	}
	
	/**
	 * This method updates the capital of the store
	 * @param amount needs to be updated to
	 */
	public void updateCapital(double amount) {
		this.capital = amount;
	}

}
